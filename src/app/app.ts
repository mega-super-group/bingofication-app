import {StorageService} from './services/storage/storage.s';
import {UserService} from './services/user/user.s';

export class App {

	private static _storage: StorageService = new StorageService();

	private static _userSvc: UserService = new UserService();

	static get storage(): StorageService {

		return App._storage;
		
	}

	static get user(): UserService {

		return App._userSvc;

	}

}
