import {Exception} from 'ts-exceptions';

export class UserEntity {

	public uid: string = null;

	public key: string = null;

	public name: string = null;

	public first: string = null;

	public last: string = null;

	public toString(): string {

		try {

			return JSON.stringify(this.toObject());

		} catch (e) {

			throw new Exception("Cannot serialize object: " + e.message, 400);

		}

	}

	public fromString(s: string): this {

		try {

			const o = JSON.parse(s);

			this.fromObject(o);
			
		} catch (e) {

			throw new Exception("Cannot read object as JSON: " + e.message, 400);

		}

		return this;

	}

	public toObject(): {[key: string]: string} {

		return {
			uid: this.uid,
			key: this.key,
			name: this.name,
			first: this.first,
			last: this.last
		};

	}

	public fromObject(obj: {[key: string]: string}): this {

		if (obj.hasOwnProperty("uid")) {
			this.uid = obj.uid;
		}
		if (obj.hasOwnProperty("key")) {
			this.key = obj.key;
		}
		if (obj.hasOwnProperty("name")) {
			this.name = obj.name;
		}
		if (obj.hasOwnProperty("first")) {
			this.first = obj.first;
		}
		if (obj.hasOwnProperty("last")) {
			this.last = obj.last;
		}

		return this;

	}

}
