import {Exception} from 'ts-exceptions';

export class StorageService {

	private storage: Storage = null;

	constructor() {

		const s: any = window.localStorage;

		if(typeof s !== "undefined") {
			this.storage = s;
			return;
		}

		throw new Exception("Application Storage can't be initialized", 500);

	}

	/**
	 *
	 * ---------------------------------------------------------------------------------------
	 * @param {string} key
	 * @param {string | number} value
	 */
	public set(key: string, value: string | number): void {

		this.storage.setItem(key, value as string);

	}

	/**
	 *
	 * ---------------------------------------------------------------------------------------
	 * @param {string} key
	 * @returns {string}
	 */
	public get(key: string): string | null {

		return this.storage.getItem(key);

	}

	/**
	 *
	 * ---------------------------------------------------------------------------------------
	 * @param {string} key
	 */
	public erase(key: string): void {

		this.storage.removeItem(key);

	}

}

export interface Storage {
	readonly length: number;
	clear(): void;
	getItem(key: string): string | null;
	key(index: number): string | null;
	removeItem(key: string): void;
	setItem(key: string, data: string): void;
	[key: string]: any;
	[index: number]: string;
}
