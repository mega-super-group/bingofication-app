import {Injectable} from '@angular/core';
import * as SIO from 'socket.io-client';
import {environment} from '../../../environments/environment';
import {GeneralMessageTypes, MessageWithPayload} from '../interfaces/message.interface';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TransportService {

	private static _socket = null;
	
	/**
	 * Pre-init static content
	 * ----------------------------------------------------------------------
	 */
	constructor() {

		this.init();

	}
	
	/**
	 * Pre initialize
	 * ----------------------------------------------------------------------
	 */
	public init(): void {

		if (TransportService._socket === null) {
			TransportService._socket = SIO(environment.gameHost);
		}

	}
	
	/**
	 * Send message through socket solution
	 * ----------------------------------------------------------------------
	 * @param type
	 * @param data
	 */
	public send<T>(type: GeneralMessageTypes, data: T): void {

		const msg: MessageWithPayload<T> = {
			id: null,
			time: Date.now(),
			data: data
		};

		TransportService._socket.emit(type, msg);

	}
	
	/**
	 * Receive message from some remote connected endpoint
	 * ----------------------------------------------------------------------
	 * @param type
	 */
	public on<T>(type: GeneralMessageTypes): Observable<MessageWithPayload<T>> {

		return new Observable<MessageWithPayload<T>>(o => {
			TransportService._socket.on(type, (data: MessageWithPayload<T>) => o.next(data));
		});

	}

}
