export class TicketEntity {

	public view: TicketElement[][] = [];

}

export class TicketElement {

	public id: number;
	public matched: boolean;

}

