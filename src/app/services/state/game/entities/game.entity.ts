import {PlayerInterface} from '../../../interfaces/player.interface';
import {GameInterface} from '../../../interfaces/game.interface';

export class GameEntity {

	public id: number = null;

	public players: PlayerInterface[] = [];

	public readyPlayers: PlayerInterface[] = [];

	constructor(gd: GameInterface) {

		this.fromGameData(gd);

	}

	public setReadyPlayers(players: PlayerInterface[]) {

		this.readyPlayers = players;

	}

	public fromGameData(gd: GameInterface) {

		if (typeof gd.players !== "undefined") {
			this.players = gd.players;
		}

		if (typeof gd.id !== "undefined") {
			this.players = gd.players;
		}

	}

}
