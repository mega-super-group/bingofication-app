import {GameEntity} from './entities/game.entity';
import {TicketEntity} from './entities/ticket.entity';
import {GameInterface} from '../../interfaces/game.interface';

export class GameStateService {

	private _inGame: GameEntity = null;

	private _ticketView: TicketEntity = null;

	private _crossed = 0;

	private _total = 0;

	constructor() {


	}

	public reset() {

		this._inGame = null;
		this._ticketView = null;
		this._crossed = 0;
		this._total = 0;

	}

	public getTicketView() {

		return this._ticketView;

	}

	public newGame(game: GameInterface) {



	}

	public isPlaying(): boolean {

		return this._inGame !== null;

	}

}
