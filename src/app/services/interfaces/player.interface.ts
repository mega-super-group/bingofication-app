import {UserInterface} from "./user.inteface";
import {TicketInterface} from './ticket.interface';

export interface PlayerInterface {
	id: number;
	ai: boolean;
	ticket: TicketInterface;
	user: UserInterface;
}
