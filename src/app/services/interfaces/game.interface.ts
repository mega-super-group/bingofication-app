import {PlayerInterface} from './player.interface';
import {TicketNumberBlockInterface} from './ticket.interface';

export interface GameInterface {

	id: number;
	createdAt: number;
	startAt: number;
	status: GameStatus;
	players: PlayerInterface[];
	maxPlayers: number;

}

export interface GameRoundInterface {
	id: number;
	playNumber: number;
}

export enum GameStatus {
	PRE_INIT,
	INIT,
	OPEN,
	STARTING,
	STARTED,
	PAUSED,
	FINISHING,
	FINISHED,
	CLOSED
}

export interface GameUpdatePreStartInterface {
	players: PlayerInterface[];
	playersReady: PlayerInterface[];
	startAtTime: number;
	untilStart: number;
}

export interface GameUpdateStartInterface extends GameInterface {

}

export interface GameUpdateNewRoundInterface extends GameRoundInterface {

}

export interface GameUpdateMetaInformationInterface {
	gameId: number;
	ticketView: TicketNumberBlockInterface[][];
}

export interface GameUpdateInRoundInterface {
	playersReady: PlayerInterface[];
	untilNextRound: number;
}

export interface GameUpdateWinnerInterface {
	players: PlayerInterface[];
}

export interface GameUpdateEndInterface {
	game: GameInterface;
	playersWon: PlayerInterface[];
	endAtTime: number;
}

export interface GameCrossNumberInterface {
	number: number;
	gameId: number;
}

export interface GameJoinInterface {
	id: number;
}

export interface GameVoteNextInterface {
	gameId: number;
}

export interface GamePlayerExitInterface {
	gameId: number;
	playerExit: PlayerInterface;
	players: PlayerInterface[];
}
