export interface TicketInterface {
	id: number;
	uid: string;
	crossed: number;
	total: number;
}

export interface TicketNumberBlockInterface {
	id: number;
	matched: boolean;
}
