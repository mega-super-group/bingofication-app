export interface UserInterface {

	uid: string;
	name: string;
	first: string;
	last: string;

}

export interface UserCredentialsInterface extends UserInterface {
	key: string;
}

