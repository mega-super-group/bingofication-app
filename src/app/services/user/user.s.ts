import {UserEntity} from '../../entities/user.entity';
import {GameStateService} from '../state/game/game.state.s';

export class UserService {

	private currentUser: UserEntity = null;

	private authD = false;

	readonly _state: GameStateService = null;

	constructor() {

		this._state = new GameStateService();

	}

	public get state(): GameStateService {

		return this._state;

	}

	public authorized(): boolean {

		return this.authD;

	}

	public getUser(): UserEntity {

		return this.currentUser;

	}

	public setUser(u: UserEntity) {

		this.currentUser = u;

	}

	public invalidate() {

		this.currentUser = null;
		this.authD = false;

	}

}
