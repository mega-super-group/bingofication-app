import {TransportService} from '../transport/transport.s';
import {Injectable} from '@angular/core';
import {GeneralMessageTypes, MessageWithPayload} from '../interfaces/message.interface';
import {Observable} from 'rxjs/Observable';
import {UserCredentialsInterface, UserInterface} from '../interfaces/user.inteface';
import {
	GameCrossNumberInterface,
	GameInterface,
	GamePlayerExitInterface,
	GameUpdateEndInterface,
	GameUpdateInRoundInterface,
	GameUpdateMetaInformationInterface,
	GameUpdateNewRoundInterface,
	GameUpdatePreStartInterface,
	GameUpdateStartInterface,
	GameUpdateWinnerInterface,
	GameVoteNextInterface
} from '../interfaces/game.interface';
import {UserIdentityMessage} from '../interfaces/user.interaction.interface';

@Injectable()
export class MessageService {

	constructor(private t: TransportService) {
		t.init();
	}

	/* -------------------------------------------------------------------------
	 *								  REQUESTS
	 * ------------------------------------------------------------------------*/

	public sendIdentification(data: UserIdentityMessage): void {

		this.t.send(GeneralMessageTypes.USER_IDENTIFY, data);

	}

	public sendNewUser(data: UserInterface): void {

		this.t.send(GeneralMessageTypes.USER_NEW_USER, data);

	}

	public sendRequestGameList(): void {

		this.t.send(GeneralMessageTypes.GAME_LIST, {});

	}

	public sendRequestGameCreate(): void {

		this.t.send(GeneralMessageTypes.GAME_NEW, {});

	}

	public sendRequestGameJoin(game: GameInterface): void {

		this.t.send<GameInterface>(GeneralMessageTypes.GAME_JOIN, game);

	}

	public sendRequestCrossNumber(number: number, game: number): void {
		
		this.t.send<GameCrossNumberInterface>(GeneralMessageTypes.GAME_CROSS_NUMBER, {
			gameId: game,
			number: number
		});

	}
	
	public sendRequestPlayerVoteNext(game: number): void {
		
		this.t.send<GameVoteNextInterface>(GeneralMessageTypes.GAME_VOTE_NEXT, {
			gameId: game
		});
		
	}
	
	public sendRequestGameExit(game: number): void {
		
		this.t.send<GamePlayerExitInterface>(GeneralMessageTypes.GAME_EXIT, {
			gameId: game,
			players: null,
			playerExit: null
		});
		
	}

	/* -------------------------------------------------------------------------
	 *								  LISTENERS
	 * ------------------------------------------------------------------------*/

	public onConfirmIdentify(): Observable<MessageWithPayload<void>> {

		return this.t.on(GeneralMessageTypes.USER_CONFIRM_IDENTIFY);

	}

	public onNewUser(): Observable<MessageWithPayload<UserCredentialsInterface>> {

		return this.t.on(GeneralMessageTypes.USER_NEW_USER);

	}

	public onAuthorization(): Observable<MessageWithPayload<UserCredentialsInterface>> {

		return this.t.on(GeneralMessageTypes.USER_ACCEPTED);

	}

	public onReceiveGameList(): Observable<MessageWithPayload<GameInterface[]>> {

		return this.t.on(GeneralMessageTypes.GAME_LIST);

	}

	public onNewGameCreated(): Observable<MessageWithPayload<GameInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_NEW);

	}

	public onGameJoin(): Observable<MessageWithPayload<GameInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_JOIN_CONFIRM);

	}
	
	public onGameOtherPlayerJoin(): Observable<MessageWithPayload<GameInterface>> {
		
		return this.t.on(GeneralMessageTypes.GAME_JOIN);
		
	}

	public onGameStart(): Observable<MessageWithPayload<GameUpdateStartInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_START);

	}

	public onGameEnd(): Observable<MessageWithPayload<GameUpdateEndInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_END);

	}

	public onGameAwaitStart(): Observable<MessageWithPayload<GameUpdatePreStartInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_PRE_START);

	}

	public onGameNewRound(): Observable<MessageWithPayload<GameUpdateNewRoundInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_NEW_ROUND);

	}

	public onGameInTheRound(): Observable<MessageWithPayload<GameUpdateInRoundInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_IN_ROUND);

	}

	public onGameMetaUpdate(): Observable<MessageWithPayload<GameUpdateMetaInformationInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_META_INF);

	}

	public onGameWinner(): Observable<MessageWithPayload<GameUpdateWinnerInterface>> {

		return this.t.on(GeneralMessageTypes.GAME_WINNER);

	}
	
	public onGamePlayerExit(): Observable<MessageWithPayload<GamePlayerExitInterface>> {
		
		return this.t.on(GeneralMessageTypes.GAME_EXIT);
		
	}

}
