import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {GameComponent} from './components/game.c';
import {GameListComponent} from './components/game.list.c';
import {CredentialsComponent} from './components/credentials.c';
import {MessageService} from './services/bingo/message.s';
import {TransportService} from './services/transport/transport.s';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {TicketComponent} from "./components/ticket.c";
import {TicketNumberComponent} from "./components/ticket.number.c.t";
import {PlayerComponent} from "./components/player.c";
import {GameFinishedComponent} from "./components/game.finished.c";

@NgModule({
	declarations: [
		AppComponent, GameComponent, GameListComponent,
		CredentialsComponent, TicketComponent, TicketNumberComponent,
		PlayerComponent, GameFinishedComponent
	],
	imports: [
		BrowserModule, FormsModule, CommonModule
	],
	providers: [TransportService, MessageService],
	bootstrap: [AppComponent]
})
export class AppModule {

}
