import { Component } from '@angular/core';
import {MessageService} from './services/bingo/message.s';
import {UserInterface} from './services/interfaces/user.inteface';
import {GameInterface, GamePlayerExitInterface, GameUpdateEndInterface} from './services/interfaces/game.interface';
import {App} from './app';
import {UserEntity} from './entities/user.entity';
import {PlayerInterface} from "./services/interfaces/player.interface";

@Component({
	selector: 'app-root',
	template: `

		<nav class="navbar navbar-light bg-dark align-content-center">
			<a class="navbar-brand align-content-center" href="#">
				<h3 class="nav-text">Bingofication!</h3>
			</a>
		</nav>
		
		<app-game-finished-component
				*ngIf="screenSelector === screenStates.GAME_RESULTS"
				[playersWon]="lastWinners"
				(exitResultsScreen)="onResultsScreenContinuePressed($event)">
			
		</app-game-finished-component>
		
		<app-credential-component 
			(playGame)="onCredentialsInput($event)" 
			*ngIf="screenSelector === screenStates.CREDENTIALS">
			
		</app-credential-component>
		
		<app-game-list-component 
			(newGameCreated)="onNewGameCreated($event)"
			(newGameJoined)="onNewGameJoined($event)"
			*ngIf="screenSelector === screenStates.GAME_SELECTION">
			
		</app-game-list-component>
		
		<app-game-component
				[game]="currentGame"
				(gameEndEvent)="onGameFinished($event)"
				(gameSurrenderEvent)="onGamePlayerExit($event)"
				*ngIf="screenSelector === screenStates.GAME_PROCESS">
			
		</app-game-component>
		
	`,
	styles: [`
	 
		:host {
			min-width: 360px;
		}
		
		.nav-text {
			color: #f8f8f8;
		}
		
	`]
})

export class AppComponent {
	
	public screenStates = GameScreenSelector;
	
	public screenSelector: GameScreenSelector = this.screenStates.INITIALIZATION;
	
	public currentGame: GameInterface = null;
	
	public lastWinners: PlayerInterface[] = [];
	
	constructor(private msg: MessageService) {

		this.mainComponentFlow();

	}
	
	/**
	 * Assign message flow for main Application component
	 * -------------------------------------------------------------------------
	 */
	public mainComponentFlow() {

		/*
		 *	Identity confirmation
		 */
		this.msg.onConfirmIdentify().subscribe(() => {

			this.msg.sendIdentification({
				uid: null,
				key: null
			});

		});

		/*
		 *  Information that new User should be created
		 */
		this.msg.onNewUser().subscribe(() => {
			
			this.screenSelector = this.screenStates.CREDENTIALS;

		});

		/*
		 *  Got authorized for some credentials
		 */
		this.msg.onAuthorization().subscribe((cred) => {

			// Set global User object
			App.user.setUser(new UserEntity().fromObject(cred.data as {}));
			
			this.screenSelector = this.screenStates.GAME_SELECTION;

		});

	}
	
	/**
	 * When User did enter his credentials and sent it to server
	 * -------------------------------------------------------------------------
	 * @param ev
	 */
	public onCredentialsInput(ev: UserInterface) {

		this.msg.sendNewUser(ev);

	}
	
	/**
	 * When User created a game and auto-join happened
	 * -------------------------------------------------------------------------
	 * @param ev
	 */
	public onNewGameCreated(ev: GameInterface) {

		console.log("New game created", ev);

		App.user.state.newGame(ev);
		this.currentGame = ev;
		this.lastWinners = [];

		this.screenSelector = this.screenStates.GAME_PROCESS;

	}
	
	/**
	 * When User was join game created by another user
	 * -------------------------------------------------------------------------
	 * @param ev
	 */
	public onNewGameJoined(ev: GameInterface) {

		console.log("New game joined", ev);

		App.user.state.newGame(ev);
		this.currentGame = ev;
		
		this.screenSelector = this.screenStates.GAME_PROCESS;

	}
	
	/**
	 * When game was finished this flow will be executed
	 * -------------------------------------------------------------------------
	 * @param ev
	 */
	public onGameFinished(ev: GameUpdateEndInterface) {

		App.user.state.reset();
		this.currentGame = null;
		this.lastWinners = ev.playersWon;
		this.screenSelector = this.screenStates.GAME_RESULTS;

	}
	
	/**
	 * When Game reported that current player did exit game
	 * -------------------------------------------------------------------------
	 * @param ev
	 */
	public onGamePlayerExit(ev: GamePlayerExitInterface) {
		
		App.user.state.reset();
		this.currentGame = null;
		
		this.lastWinners = [];
		
		this.screenSelector = this.screenStates.GAME_RESULTS;
		
	}
	
	/**
	 * When Game results screen has pressed continue button
	 * -------------------------------------------------------------------------
	 * @param ev
	 */
	public onResultsScreenContinuePressed(ev: void) {
		
		this.screenSelector = this.screenStates.GAME_SELECTION;
		
	}

}

export enum GameScreenSelector {
	NONE,
	INITIALIZATION,
	CREDENTIALS,
	GAME_SELECTION,
	GAME_PROCESS,
	GAME_RESULTS
}


