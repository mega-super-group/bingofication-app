import {Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output} from "@angular/core";
import {TicketNumberBlockInterface} from "../services/interfaces/ticket.interface";

@Component({
	selector: 'app-ticket-number-component',
	template: `
	
		<div class="" (click)="onCrossOut()">
			
			<div class="closer" *ngIf="numberBlock.matched">
				<i class="fa fa-close"></i>
			</div>
			
			<div>
				{{numberBlock?.id}}
			</div>
			
		</div>
	
	`,
	styles: [`
		
		:host {
			width: 32px;
			height: 32px;
			display: inline-block;
		}
		
		.closer {
			position: absolute;
            width: 32px;
            height: 32px;
			font-size: 1.5em;
		}
		
	`]
})

export class TicketNumberComponent implements OnInit, OnDestroy {
	
	@HostBinding('class') public hostClass = "col-2";
	
	@Input() public numberBlock: TicketNumberBlockInterface = null;
	
	@Output() public crossOut: EventEmitter<TicketNumberBlockInterface> = new EventEmitter();
	
	constructor() {
		
	}
	
	public onCrossOut() {
		
		this.crossOut.emit(this.numberBlock);
		
	}
	
	ngOnDestroy(): void {
	
	}
	
	ngOnInit(): void {
	
	}
	
}
