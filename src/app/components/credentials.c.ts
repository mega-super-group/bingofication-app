import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserInterface} from '../services/interfaces/user.inteface';

@Component({
	selector: 'app-credential-component',
	template: `
		
		<div class="container-fluid middle w-100">
			
			<form class="form-middle">
				
				<div class="form-group row">
					<label for="inp-nickname" class="col-3 col-form-label">Nickname</label>
					<div class="col-9">
						<input type="text" 
							   class="form-control" 
							   id="inp-nickname"
							   [(ngModel)]="name"
							   [ngModelOptions]="{standalone: true}"
							   placeholder="Name">
					</div>
				</div>
				
				<!--<div class="form-group row">
					<label for="inputPassword3" class="col-sm-2 col-form-label">First</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="form-control" 
							id="inputPassword3" 
							[(ngModel)]="first"
							[ngModelOptions]="{standalone: true}"
							placeholder="First name">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-2 col-form-label">Last</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="form-control" 
							id="inputPassword3" 
							[(ngModel)]="last"
							[ngModelOptions]="{standalone: true}"
							placeholder="Last name">
					</div>
				</div>-->
			
				<div class="form-group row">
					<div class="col-lg-12">
						<button class="btn btn-primary btn-block" (click)="play()">Play!</button>
					</div>
				</div>
				
			</form>
			
		</div>
		
	`,
	styles: [`
		
		.middle {
			
			position: absolute;
			top: 50%;
			margin-top: -100px;
			text-align: center;
			
		}
		
		.form-middle {
			
			display: inline-block;
			min-width: 310px;
			
		}
		
	`]
})

export class CredentialsComponent implements OnInit {

	@Output() public playGame: EventEmitter<UserInterface> = new EventEmitter();

	public name = "";
	public first  = "";
	public last  = "";

	constructor() {

	}

	ngOnInit() {

	}

	public play() {

		this.playGame.emit({
			uid: null,
			name: this.name,
			first: this.first,
			last: this.last
		});

	}

}
