import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GameInterface} from '../services/interfaces/game.interface';
import {MessageService} from '../services/bingo/message.s';

@Component({
	selector: 'app-game-list-component',
	template: `

		<div class="container">

			<div class="row">

				<div class="col-12 padded-spacer">
				
				</div>
				
			</div>
			
			<div class="row">

				<div class="col-8">
					<h2> Games available </h2>
				</div>
				
				<div class="col-4">
					<button
							class="btn btn-primary pull-right"
							(click)="onNewGame()">
						New game
					</button>
				</div>
				
			</div>
			
			<div class="row">
				
				<div class="col-2"></div>
				<div class="col-8">

					<ul class="list-group">

						<li class="list-group-item"
							*ngFor="let i of gameList">
							
							<div class="row">

								<div class="col-6">
									Game #{{i.id}} starts at {{gameStartToSeconds(i.startAt)}}
								</div>

								<div class="col-6">
									<button class="btn btn-primary pull-right"
									        (click)="clickJoinGame(i)">
										Join
									</button>
								</div>
								
							</div>
							
						</li>

					</ul>
					
				</div>
				<div class="col-2"></div>
				
			</div>
			
		</div>
		
	`,
	styles: [`
		.padded-spacer {
			padding-top: 10px;
            padding-bottom: 10px;
		}
	`]
})

export class GameListComponent implements OnInit {

	@Output() public newGameCreated: EventEmitter<GameInterface> = new EventEmitter();

	@Output() public newGameJoined: EventEmitter<GameInterface> = new EventEmitter();

	public gameList: GameInterface[] = [];

	constructor(private msg: MessageService) {

		this.msg.onNewGameCreated().subscribe(game => {

			this.newGameCreated.emit(game.data);

		});

		this.msg.onReceiveGameList().subscribe(list => {

			this.gameList = list.data;

		});

		this.msg.onGameJoin().subscribe(ev => {

			console.log("GAME JOIN MESSAGE CAME EMITTING", ev);
			this.newGameJoined.emit(ev.data);

		});

		this.refreshGames();

	}

	ngOnInit() {

	}

	public onNewGame() {

		this.msg.sendRequestGameCreate();

	}
	
	public gameStartToSeconds(start: number) {
		
		return Math.floor((start - Date.now()) / 1000);
		
	}

	public refreshGames() {

		const iv = setInterval(() => {

			this.msg.sendRequestGameList();

		}, 2000);

	}

	public clickJoinGame(game: GameInterface) {

		this.msg.sendRequestGameJoin(game);

	}

}
