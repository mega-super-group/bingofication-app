import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {TicketInterface, TicketNumberBlockInterface} from "../services/interfaces/ticket.interface";
import {MessageService} from "../services/bingo/message.s";
import {Subscription} from "rxjs/Subscription";
import {GameInterface} from "../services/interfaces/game.interface";

@Component({
	selector: 'app-ticket-component',
	template: `
		
		<div class="card ticket-bgr">
			
			<div class="container">
				
				<div class="row">

					<div class="col-md-4"><h4>TICKET</h4></div>
					<div class="col-md-8"><h5>#{{_ticket?.uid}}</h5></div>
					
				</div>
				
				<div class="row">
					
					<div class="col-lg-12">

						<div class="row" *ngFor="let rows of _view">

							<app-ticket-number-component
									(crossOut)="onCrossOut($event)"
									*ngFor="let cell of rows"
									[numberBlock]="cell">

							</app-ticket-number-component>

						</div>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
	
	`,
	styles: [`
	
		.ticket-bgr {
            background: #ffe259;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #ffa751, #ffe259);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #ffa751, #ffe259); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }
		
	`]
})

export class TicketComponent implements OnDestroy {
	
	@Input() public game: GameInterface = null;
	
	public _ticket: TicketInterface = null;
	
	public _view: TicketNumberBlockInterface[][] = [];
	
	@Input() set ticket(t: TicketInterface) {
		
		this._ticket = t;
		
	}
	
	public subs: Subscription[] = [];
	
	constructor(private msg: MessageService) {
	
		this.flow();
		
	}
	
	ngOnDestroy(): void {
		
		for (const s of this.subs) {
			s.unsubscribe();
		}
		
	}
	
	public onCrossOut(ev: TicketNumberBlockInterface) {
	
		this.msg.sendRequestCrossNumber(ev.id, this.game.id);
	
	}
	
	public flow() {
		
		this.subs.push(this.msg.onGameMetaUpdate().subscribe(u => {
		
			this._view = u.data.ticketView;
		
		}));
		
	}
	
	
}
