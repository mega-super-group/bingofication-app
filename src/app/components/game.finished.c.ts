import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from "@angular/core";
import {PlayerInterface} from "../services/interfaces/player.interface";
import {App} from "../app";

@Component({
	selector: 'app-game-finished-component',
	template: `
	
		<!--Elements to show Winner screen-->
		<div class="container" *ngIf="ready && winner">
			
			<div class="row">
				
				<div class="col-md-2">
				
				</div>

				<div class="col-md-8 text-center">
					
					<h2> CONGRATULATIONS </h2>
					<img class="mw-100" src="assets/cup.png"/>

					<div *ngIf="_playersWon.length > 0">

						<ul class="list-group">

							<li class="list-group-item"
							    *ngFor="let player of _playersWon">
								{{player?.user?.name}}
							</li>

						</ul>

					</div>
					
				</div>

				<div class="col-md-2">

				</div>
				
			</div>

			<div class="row">

				<div class="col-md-2"></div>

				<div class="col-md-8 text-center pt-4">
					<button
							class="btn btn-primary btn-lg mw-100"
					        (click)="nextScreen($event)">
						Continue
					</button>
				</div>

				<div class="col-md-2"></div>
				
			</div>
			
		</div>

		<!--Elements to show loser screen-->
		<div class="container" *ngIf="ready && !winner">

			<div class="row">

				<div class="col-md-2"></div>

				<div class="col-md-8 text-center">

					<h2> NOT TODAY </h2>
					<img class="mw-100" src="assets/poo.png"/>
					
					<div *ngIf="_playersWon.length > 0">

						<ul class="list-group">

							<li class="list-group-item"
							    *ngFor="let player of _playersWon">
								{{player?.user?.name}}
							</li>

						</ul>
						
					</div>
					
				</div>

				<div class="col-md-2"></div>

			</div>

			<div class="row">

				<div class="col-md-2"></div>
				
				<div class="col-md-8 text-center pt-4">
					<button
							class="btn btn-primary btn-lg mw-100"
							(click)="nextScreen()">
						Continue
					</button>
				</div>

				<div class="col-md-2"></div>

			</div>

		</div>
		
	`,
	styles: [`
	
	
	
	`]
	
})

export class GameFinishedComponent {
	
	public ready = false;
	
	public winner = false;
	
	public _playersWon: PlayerInterface[] = [];
	
	@Input() public set playersWon(p: PlayerInterface[]) {
	
		this.distributePlayers(p);
		
		this.ready = true;
	
	}
	
	@Output() public exitResultsScreen: EventEmitter<void> = new EventEmitter();
	
	/**
	 * Distribute players to determine if current user is winner
	 * ----------------------------------------------------------------------
	 * @param p
	 */
	public distributePlayers(p: PlayerInterface[] = []) {
	
		this._playersWon = p.filter(pl => {
			
			if (App.user.getUser().uid === pl.user.uid) {
				
				this.winner = true;
				
			}
			
			return true;
			
		});
		
	}
	
	/**
	 * Event applied on Continue button
	 * ----------------------------------------------------------------------
	 */
	public nextScreen() {
		
		this.exitResultsScreen.emit();
		
	}
	
}
