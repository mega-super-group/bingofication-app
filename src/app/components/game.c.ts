import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {MessageService} from '../services/bingo/message.s';
import {Subscription} from 'rxjs/Subscription';
import {GameInterface, GamePlayerExitInterface, GameRoundInterface, GameUpdateEndInterface} from '../services/interfaces/game.interface';
import {PlayerInterface} from "../services/interfaces/player.interface";
import {App} from "../app";

@Component({
	selector: 'app-game-component',
	template: `

		<div class="container">
			
			<div class="row">
				
				<div class="col-lg-12">

					<h2>
						Game {{_game !== null ? _game.id : ''}}
						<span *ngIf="howMuchUntilStart() > 0">
						starting in {{howMuchUntilStart()}}
						</span>
					</h2>
					
				</div>

				<div class="col-lg-12">
					
					<ul class="rounds">
						<li class="round"
						    *ngFor="let r of _rounds">{{r.playNumber}}
						</li>
					</ul>
					
				</div>

				<div class="col-lg-12">
					
					<div class="row">

						<div class="col-lg-12">

							<h3>{{_announcement}}</h3>

						</div>

					</div>

					<app-player-component
							*ngFor="let p of _players"
							[game]="_game"
							[player]="p">
					</app-player-component>

					<app-player-component
							[_currentPlayer]="true"
							[game]="_game"
							[player]="_currentPlayer">
					</app-player-component>

					<ul class="list-group">

						<li class="list-group-item no-overflow"
						    *ngFor="let i of tempEvents">{{i}}
						</li>

					</ul>
					
				</div>
				
			</div>
			
		</div>
		
	
	`,
	styles: [`
		
		.rounds {
			list-style-type: none;
		}
		
		.round {
			display: inline-block;
			padding: 4px;
			margin-right: 2px;
			background-color: azure;
		}
		
		.no-overflow {
			overflow: hidden;
			font-size: 1em;
			word-break: break-all;
		}
		
	`]
	
})

export class GameComponent implements OnInit, OnDestroy {
	
	public _game: GameInterface = null;
	
	/**
	 * Game setter
	 * ----------------------------------------------------------------------
	 * All the parameters which should be wiped down on a new game
	 * will be resetting in this method
	 * @param g
	 */
	@Input() public set game(g: GameInterface) {
		
		if (g === null) {
			return;
		}
		
		this._rounds = [];
		this._players = [];
		this._currentPlayer = null;
		this.tempEvents = [];
		this._game = g;
		
		this.distributePlayers(g.players);
		
	}
	
	@Output() public gameEndEvent: EventEmitter<GameUpdateEndInterface> = new EventEmitter();
	
	@Output() public gameSurrenderEvent: EventEmitter<GamePlayerExitInterface> = new EventEmitter();
	
	public _players: PlayerInterface[] = [];
	
	public _currentPlayer: PlayerInterface = null;
	
	public _rounds: GameRoundInterface[] = [];
	
	public _announcement = "";

	public subs: Subscription[] = [];

	public tempEvents: string[] = [];

	public untilStart = 0;
	
	/**
	 * Construct with DI of Message Service
	 * ----------------------------------------------------------------------
	 * @param msg
	 */
	constructor(private msg: MessageService) {

		this.componentFlow();

	}
	
	/**
	 *  How much seconds actually until this game will start
	 *  ----------------------------------------------------------------------
	 */
	public howMuchUntilStart() {
		
		return Math.floor(this.untilStart / 1000);
		
	}
	
	/**
	 *  Prior component being rendered
	 *  ----------------------------------------------------------------------
	 */
	ngOnInit() {

	}
	
	/**
	 *  If component goes to be destroyed
	 *  ----------------------------------------------------------------------
	 *  Unbind subscribers
	 */
	ngOnDestroy() {

		for(const s of this.subs) {
			s.unsubscribe();
		}

	}
	
	/**
	 *  Will update round announcement
	 *  ----------------------------------------------------------------------
	 */
	public updateAnnouncement() {
		
		const phrases = [
			"Aaaaaanndddd here weeee goooooo withhhhh: ",
			"Nooooowwwww theeeeee nummmbeeeerrrr isssss: ",
			"Whaaaaaaattt weeeee havvvvvinnngggg heeeerreee: ",
			"Maybe we willllll knowwww theee winnneer becaaauseeee nummmber issss:",
			"Sooooo whoooo wwaaaannntt tooo crossss theee nummmmbeer: ",
			"When you need to call — better call Saul!: ",
			"Our nexxxxxxt nummmmberrr is: ",
			"I bet we will be having bingo after this number: ",
			"The best one number in your life is: ",
			"Remember yourself at this age: ",
			"Abracadabra! Next is: "
		];
		
		const phrase = phrases[Math.floor(Math.random() * phrases.length)];
		
		this._announcement = phrase + this._rounds[this._rounds.length - 1].playNumber;
		
	}
	
	/**
	 *  Game component flow
	 *  ----------------------------------------------------------------------
	 *  Flow of events and actions they produce
	 */
	public componentFlow() {

		/*
		 *  Listen for events happened prior game start
		 */
		const awaitStartL = this.msg.onGameAwaitStart().subscribe(ev => {

			this.untilStart = ev.data.untilStart;

		});

		/*
		 *  Listen for Game start events
		 */
		const startedL = this.msg.onGameStart().subscribe(ev => {

			this.pushLog("Game started" + ev.id);
			this.untilStart = 0;

		});

		/*
		 *  Listen for in-round events broadcaast
		 */
		const inRoundL = this.msg.onGameInTheRound().subscribe(ev => {

			this.pushLog("We in round yet, everybody deciding: " + ev.data.untilNextRound);

		});
		
		/*
		 *  Listen for new Round requests
		 */
		const newRoundL = this.msg.onGameNewRound().subscribe(ev => {

			this._rounds.push(ev.data);
			this.pushLog("New round started: " + ev.data.id + " Number is: " + ev.data.playNumber);
			this.updateAnnouncement();
			
		});

		/*
		 *  Listen for Game winner broadcast
		 */
		const winnerL = this.msg.onGameWinner().subscribe(ev => {

			this.pushLog("Game has a winner: " + JSON.stringify(ev.data.players[0]));

		});
		
		/*
		 *  Listen when Game will come to an End
		 */
		const endL = this.msg.onGameEnd().subscribe(ev => {

			this.pushLog("Game ended: " + JSON.stringify(ev.data.playersWon[0]));

			setTimeout(() => {
				
				this.gameEndEvent.emit(ev.data);
				
			}, 3000);

		});
		
		/*
		 *  Listen for Ticket View events
		 */
		const onTicketViewL = this.msg.onGameMetaUpdate().subscribe(ev => {

			const str = [];

			for(let i = 0; i < ev.data.ticketView.length; i++) {

				const row = ev.data.ticketView[i];

				for (let j = 0; j < row.length; j++) {

					const current = row[j];

					str.push(current.id + '' + (current.matched ? 'x' : ''));
				}

			}

			this.pushLog("Your ticket: " + JSON.stringify(str));

		});
		
		/*
		 *  Listen for Game Joins
		 */
		const onGameJoinL = this.msg.onGameOtherPlayerJoin().subscribe(ev => {
			
			this.distributePlayers(ev.data.players);
			
		});
		
		/*
		 *  Listen for Game exit
		 */
		const onGamePlayerExitL = this.msg.onGamePlayerExit().subscribe(ev => {

			if (ev.data.playerExit.user.uid === App.user.getUser().uid) {
				
				this.gameSurrenderEvent.emit({
					gameId: this._game.id,
					playerExit: ev.data.playerExit,
					players: ev.data.players
				});
				
				return;
				
			}
			
			this.distributePlayers(ev.data.players);
		
		});
		
		// Append all listeners for be able to be unsubscribed
		this.subs = [awaitStartL, startedL, inRoundL,
			newRoundL, winnerL, endL, inRoundL, onTicketViewL,
			onGameJoinL, onGamePlayerExitL
		];
		
	}
	
	/**
	 * Distribute players into current player and just players
	 * ----------------------------------------------------------------------
	 * @param players
	 */
	private distributePlayers(players: PlayerInterface[]) {
		
		players.map(p => {
			
			if (p.user.uid === App.user.getUser().uid) {
				
				this._currentPlayer = p;
				
			} else {
				
				this._players.push(p);
				
			}
			
		});
		
	}
	
	/**
	 * Push to console of the game
	 * ----------------------------------------------------------------------
	 * @param message
	 */
	private pushLog(message: string) {
		
		if (this.tempEvents.length > 9) {
			this.tempEvents.shift();
		}
		
		this.tempEvents.push(message);
		
	}

}
