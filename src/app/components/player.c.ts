import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {PlayerInterface} from "../services/interfaces/player.interface";
import {TicketInterface} from "../services/interfaces/ticket.interface";
import {GameInterface} from "../services/interfaces/game.interface";
import {MessageService} from "../services/bingo/message.s";
import {Subscription} from "rxjs/Subscription";

@Component({
	selector: 'app-player-component',
	template: `

		<div class="row">

			<div class="col-12 card align-content-center">

				<div class="container player-bucket">

					<div class="row">

						<div class="col-6">

							<h3>
								<i class="fa fa-user-circle" aria-hidden="true"></i>
								{{_player?.user?.name}}
							</h3>

						</div>

						<div class="col-6">

							<button class="btn btn-primary pull-right"
							        *ngIf="!isReadyForNext && _currentPlayer"
							        (click)="voteForNext()">
								Vote for next number
							</button>

						</div>

					</div>

					<app-ticket-component
							[ticket]="_ticket"
							[game]="game"
							*ngIf="_currentPlayer">

					</app-ticket-component>

					<div class="ticket-spacer"></div>

					<div class="row">

						<div class="col-12">
							<button class="btn btn-danger"
							        (click)="surrenderTheGame()"
							        *ngIf="_currentPlayer">
								Surrender the game
							</button>
						</div>
						
					</div>
					
				</div>
				
			</div>

		</div>
	
	`,
	styles: [`
	
		.player-bucket {
			margin: 5px;
			max-width: 600px;
			padding: 10px;
			display: inline-block;
		}
		
		.ticket-spacer {
			width: 100%;
			height: 10px;
		}
		
		.card {
            flex-direction: initial !important;
			justify-content: center;
		}
		
	`]
})

export class PlayerComponent implements OnInit, OnDestroy {
	
	@Input() public _currentPlayer = false;
	
	public _ticket: TicketInterface = null;
	
	@Input() public game: GameInterface = null;
	
	public _player: PlayerInterface = null;
	
	/**
	 * Set new player object
	 * ----------------------------------------------------------------------
	 * This will reset whole component to carry new object
	 * @param p
	 */
	@Input() public set player(p: PlayerInterface) {
		
		if (p === null) {
			return;
		}
		
		this._player = p;
		
		this.ticket = p.ticket;
		
	}
	
	/**
	 * Set ticket for current player
	 * ----------------------------------------------------------------------
	 * @param t
	 */
	@Input() public set ticket(t: TicketInterface) {
		
		this._ticket = t;
		
	}
	
	public isReadyForNext = false;
	
	public updateListener: Subscription = null;
	
	/**
	 * Construct with DI of message service
	 * ----------------------------------------------------------------------
	 * @param msg
	 */
	constructor(private msg: MessageService) {
	
		this.componentFlow();
		
	}
	
	/**
	 * Execute actions on when component destroys
	 * ----------------------------------------------------------------------
	 */
	ngOnDestroy(): void {
	
		this.updateListener.unsubscribe();
		
	}
	
	/**
	 * Execute action on component initialize
	 * ----------------------------------------------------------------------
	 */
	ngOnInit(): void {
	
	}
	
	/**
	 * Player votes for next action to happen
	 * ----------------------------------------------------------------------
	 */
	public voteForNext() {
	
		this.msg.sendRequestPlayerVoteNext(this.game.id);
	
	}
	
	/**
	 * Player votes to leave the game
	 * ----------------------------------------------------------------------
	 */
	public surrenderTheGame() {
		
		this.msg.sendRequestGameExit(this.game.id);
		
	}
	
	/**
	 *  Player component flow
	 *  ----------------------------------------------------------------------
	 *  Flow of events and actions they produce
	 */
	public componentFlow() {
		
		this.updateListener = this.msg.onGameInTheRound().subscribe(u => {
		
			const playersReady = u.data.playersReady;
			
			for (const player of playersReady) {
				
				if (player.id === this._player.id) {
					this.isReadyForNext = true;
					return;
				}
				
			}
			
			this.isReadyForNext = false;
		
		});
		
	}
	
}
