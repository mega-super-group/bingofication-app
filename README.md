# Bingofication App

## How to start

Please read all startup scripts from [package.json](package.json) file.

#### Local start

Simply execute start-local script

```
npm run start-local
```

#### Local dockerized start

Having docker installed execute following scripts

Build
```
sh build-local.sh
```

Deploy
```
sh deploy-local.sh
```

#### Remote dockerized start

Copy and edit following files:

[build-staging](build-staging.sh)

[deploy-staging](deploy-staging.sh)

For set up those for your docker cluster

## Project structure

```
root
|
|-- src|
|      |-- app|
|      |      |-- components
|      |      |-- entities
|      |      |-- services|
|      |                  |-- bingo
|      |                  |-- interfaces
|      |                  |-- state
|      |                  |-- storage
|      |                  |-- transport
|      |                  |-- user
|      |-- assets
|      |-- envoronments
|      |- index.html
|      |- main.ts
|
|- package.json
|- *.sh scripts

```

#### Components

Application View components,
combination of HTML and code bindings

#### Entities

Entities which application operates,
usually representation of some model data

#### Services

Services handles some interaction logic
for this Application can receive data,
distribute data and manipulate data

* `bingo` services — services related to
particular application logic related to
bingo server game engine

* `interfaces` — Interfaces provides compatibility
of objects between server and client

* `state` — services are for maintain some
state for some entity at current moment

* `storage` — storage is a services which
provides interface to store or retrieve data
from different persistence state endpoints

* `transport` - is a service which provides and
abstraction to build interaction layer for
operations between server and client

* `user` - service allows interaction with
user entity to store or read data

## Bootstrap me

Chain of events on how this program starts
is as following:

0) compile/build according to `/tsconfig.json`

1) got to `src/index.html`

2) launched after `src/main.ts`

3) got module prior `src/app/app.module.ts`

4) got component after module `src/app/app.component.ts`