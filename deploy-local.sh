#!/usr/bin/env bash
eval $(docker-machine env -u)
docker container stop bingo-app
docker container rm bingo-app
docker run -p 8080:8080 -d --name bingo-app local/bingo-app:latest